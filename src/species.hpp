#pragma once
#include "genom/genom.hpp"
#include "neuralnetwork/nnet.hpp"
#include "public_values.h"
#include <map>
#include <list>

extern float AverageScore;

class species
{
private:
    genom* gRef;
    std::list<genom> genoms;
public:
    const genom* getGRef() const;
    bool addGenom(genom &&genom_);
    bool addGenom(const genom &other);
    bool addGenom(const std::list<genom>::iterator &srcElement, std::list<genom> &srcList);
    void trim();
    void createNNs();
    //create children for every genom in this species and return the size of the genom before creating children
    void getChildren(std::list<genom> &children);
    void calculateScores();
    species(genom &&initialGenom);
    species(const genom &initialGenom);
    species(const std::list<genom>::iterator &srcElement, std::list<genom> &srcList);
};
