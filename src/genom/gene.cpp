#include "gene.hpp"

pGene::pGene(const uint &in, const uint &out) : in(in), out(out), child(PublicGenom.NullChild()){}
pGene::~pGene(){}


const uint& pGene::GetInnovation() const { return innovationNumber; }
const uint& pGene::GetIn () const { return in ; }
const uint& pGene::GetOut() const { return out; }
const pGene &pGene::GetChildPrev() const { return *child; }
const pGene &pGene::GetChildNext() const { return *(std::next(child)); }


gene::gene(pGene *gene_, const float &weight) : innovationNumber(gene_), weight(weight), enabled(true) {} 
const uint& gene::GetIn() const { return innovationNumber->GetIn(); }
const uint& gene::GetOut() const { return innovationNumber->GetOut(); }
void gene::Disable() { enabled = false; }
const bool& gene::IsEnabled() const { return enabled; }
const float& gene::GetWeight() const { return weight; }
const uint& gene::GetInnovationNumber() const { return innovationNumber->GetInnovation(); }


bool gene::operator<(const gene &other) const {
    return this->innovationNumber < other.innovationNumber;
}

void gene::mutateNode(genom *parent) {
    if (!enabled)
        return;

    enabled = false;
    
    PublicGenom.addNode(innovationNumber);

    parent->addGene(gene(&(*(innovationNumber->child)), weight));
    parent->addGene(gene(&(*std::next(innovationNumber->child)), 1));
}

void gene::ShiftWeight(){
    weight += RandFloat(-1*MaxWeightShift, MaxWeightShift);
}

void gene::RandomizeWeight() {
    weight = RandFloat(-1 * MaxRandomWeight, MaxRandomWeight);
}


bool pGene::operator==(const pGene &other) const {
    return in == other.in && out == other.out;
}

const uint& gene::GetNext() const {
    return (++(innovationNumber->child))->GetInnovation();
}

const uint &gene::GetPrev() const {
    return innovationNumber->child->GetInnovation();
}