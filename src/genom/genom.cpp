#include "genom.hpp"
#include "../public_values.h"
#include <iostream>
#include "../species.hpp"

std::list<NeuralNetwork>::iterator genom::getChild() { return child; }
const float& genom::getScore() const { return score; }

void genom::calculateScore() {
    score = child->score/overallCompabilitty();
    AverageScore += score;
}

float genom::overallCompabilitty() const {
    float compabilittyScore = 0;
    for (auto i = Species.begin(); i != Species.end(); i++)
    {
        //!not how the neat paper recommends it since I think this approach is more usefull. No discredit to the neat authors they did a great job and it is easy to argue towards their approach 
       compabilittyScore += this->difference(*i->getGRef());   
    }
    return compabilittyScore;
}

void genom::Print() const {
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        std::cout << "in:" << i->second.GetIn() << "out:" << i->second.GetOut() << "\n\n";
    }   
}

genom genom::crossover(const genom &other) {
    genom child;
    auto otherIt = other.genes.begin();
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        for(otherIt; *otherIt < *i && otherIt != other.genes.end(); otherIt++)
            child.genes.emplace_hint(child.genes.end()--, *otherIt);
    
        if( otherIt->first == i->first) {
            if ( RandBool() ) {
                child.genes.emplace_hint(child.genes.end()--, *i);
            }else{
                child.genes.emplace_hint(child.genes.end()--, *otherIt);
            }
            if ( !(otherIt->second.IsEnabled() && i->second.IsEnabled())) {
                child.genes.rbegin()->second.Disable();
            }
        }else{
            child.genes.emplace_hint(child.genes.end()--, *otherIt);
        }
    }

    return child; 
}

void genom::addGene(gene &&gene_) {
    genes.insert(std::make_pair(gene_.GetInnovationNumber(), gene_));
}

genom::genom(genom &&other) {
    genes = std::move(other.genes);
    child = other.child;
    other.child = NNPopulation.end();
}

genom::genom(const genom &other) {
    genes = other.genes;
    child = NNPopulation.end();
}

void genom::addChild() {
    NNPopulation.emplace_front(this);
    if( child != NNPopulation.end()) {
        std::cout << "err: multiple construction of nn from this genom\n";
        return;
    }
    child = NNPopulation.begin();
}

void genom::mutateNode() {
    auto index = RandInt(genes.size())+1;
    auto iter = genes.begin();

    for (size_t i = 0; i < index; i++, iter++){}
        iter->second.mutateNode(this);
}

inline uint genom::mutateLinkOut(const std::set<uint> &nodes) const {
    uint out = RandInt(nodes.size()+GlobalOutputCount);
    if (out >= nodes.size()) {
        return out - nodes.size() + GlobalInputCount;
    }

    return *std::next(nodes.begin(), out);
}

inline uint genom::mutateLinkIn(const std::set<uint> &nodes) const {
    uint in = RandInt(nodes.size()+GlobalInputCount);
    if (in >= nodes.size())
        return in-nodes.size();

    return *std::next(nodes.begin(), in);
}

inline void genom::After(std::set<uint> &nodes, const uint &in) const {
    std::multimap<uint, uint> structure;

    for (auto i = genes.begin(); i != genes.end(); i++)
        if(i->second.IsEnabled())
            structure.insert(std::make_pair(i->second.GetOut(), i->second.GetIn()));

    auto it = structure.find(in);

    for (it; it->first == in && it != structure.end(); it++)
        TraceRoot(structure, nodes, it); 

    nodes.erase(in);   
}

void genom::TraceRoot(std::multimap<uint, uint> &structure, std::set<uint> &nodes, const std::multimap<uint, uint>::iterator &in) const {
    if(in->second <= GlobalInputCount)//either invalid or an input node
        return;
    for (auto it = structure.find(in->second); it->first == in->second && it != structure.end(); it++)
        TraceRoot(structure, nodes, it);

    nodes.erase(in->second);

    in->second = GlobalInputCount;//invalidate
}

void genom::mutateLink() {
    auto nodes = getNodes();
    uint in = mutateLinkIn(nodes);
    After(nodes, in);//trim nodes
    uint out = mutateLinkOut(nodes);

    gene tempGene(PublicGenom.getGene(pGene(in, out)), RandFloat(-1* MaxRandomWeight, MaxRandomWeight));
    genes.insert(std::make_pair(tempGene.GetInnovationNumber(), tempGene));
}

void genom::mutateWeight() {
    auto index = RandInt(genes.size());

    std::next(genes.begin(), index)->second.RandomizeWeight();
}

void genom::mutate() {
    if (RandFloat(0, 1) < MutateLinkP)
        this->mutateLink();
    
    if (RandFloat(0, 1) < MutateNodeP)
        this->mutateNode();

    if (RandFloat(0, 1) < RandomWeightP)
        this->mutateWeight();
    
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        i->second.ShiftWeight();
    }
    
}

float genom::difference(const genom &other) const {
    auto otherIt = other.genes.begin();
    float diffStructure = 0;
    float diffWeight = 0;
    float EqualGenes = 0;
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        for (otherIt; std::next(otherIt)->first < i->first && otherIt != other.genes.end(); otherIt++)
        {
            diffStructure += DisjointGeneImportance;       
        }
        if(std::next(otherIt)->first == i->first)
            otherIt++;
        if ( otherIt->first == i->first ) {
            EqualGenes++;
            diffWeight += WeightImportance*GetDifference(i->second.GetWeight(), otherIt->second.GetWeight());
        }else if (otherIt == other.genes.end()){
            diffStructure += ExcessGeneImportance;
        }else{
            diffStructure += DisjointGeneImportance;
        }
    }
    otherIt++;
    for (otherIt; otherIt != other.genes.end(); otherIt++)
        diffStructure += ExcessGeneImportance;
    
    return (diffWeight / EqualGenes) + (diffStructure / float(other.genes.size()+genes.size()));
}

//void genom::setChild(const std::list<NeuralNetwork>::iterator &child_) {
//    child = child_;
//}

std::set<uint> genom::getNodes() const {
    std::set<uint> nodes;
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        if(i->second.GetOut() >= GlobalNonHiddenNodeCount)
            nodes.insert(i->second.GetOut());        
    }
    return nodes;
}

const std::map<uint ,gene>& genom::getGenes() const {
    return genes;
}

genom::genom() : genes(PublicGenom.CreateCompleteGenom()) {
    child = NNPopulation.end();
}//initial genome

genom::~genom() {
    if(child != NNPopulation.end())
        NNPopulation.erase(child);
}