#include "pGenom.hpp"

pGenom::pGenom() {}
std::list<pGene>::iterator pGenom::NullChild() { return genes.end(); }

void pGenom::Init() {
    for (size_t i = 0; i < GlobalInputCount; i++)
        for (size_t j = 0; j < GlobalOutputCount; j++) {
            genes.emplace_back(i, j + GlobalInputCount);
            genes.back().innovationNumber = Innovations;
            Innovations++;
        }
}

std::map<uint, gene> pGenom::CreateCompleteGenom() {
    std::map<uint, gene> privateGenes;
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        privateGenes.emplace_hint(privateGenes.rbegin().base() , i->GetInnovation(), gene(&(*i), RandFloat(0-MaxRandomWeight, MaxRandomWeight)));
    }
    return privateGenes;
}

pGene* pGenom::getGene(const pGene &gene_) {
    for (auto i = genes.begin(); i != genes.end(); i++)
        if (*i == gene_)
            return &(*i);

    genes.push_back(gene_);
    genes.back().innovationNumber = Innovations;
    Innovations++;
    return &genes.back();
}


void pGenom::addNode(pGene *gene_) {
    if (gene_->child == genes.end()) {    
        genes.emplace_back(gene_->in, GlobalNodeCount);
        gene_->child = genes.end();
        gene_->child--;
        genes.emplace_back(GlobalNodeCount, gene_->out);
        gene_->child->innovationNumber = Innovations;
        Innovations++;
        genes.back().innovationNumber = Innovations;
        Innovations++;
        GlobalNodeCount++;
    }
}

void pGenom::Print() const {
    for (auto i = genes.begin(); i != genes.end(); i++)
    {
        std::cout << "in:" << i->in << "\nout:" << i->out << "\ninnovation:" << i->GetInnovation() << "\n\n";
    }
    
}

