#include "sigmoid.hpp"

float Sigmoid(const float &value) {
    return 1 / (1 + exp(-value));
}