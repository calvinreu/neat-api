#include "random.hpp"

float RandFloat(const float &min, const float &max) {
    return ((float(std::rand())/RAND_MAX)*(max-min))+min;
}

int RandInt(const int &max) {
    return std::rand()%max;
}

bool RandBool() {
    return std::rand()%2;
}