#include "difference.hpp"

float GetDifference(const float &first, const float &second) {
    if ( first < second)
        return second - first;
    else
        return first - second;
}