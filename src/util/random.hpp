#pragma once
#include <random>

extern float RandFloat(const float &min, const float &max);
extern bool RandBool();
extern int RandInt(const int &max);