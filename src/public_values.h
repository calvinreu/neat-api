#pragma once

#include "genom/genom.hpp"
#include "genom/pGenom.hpp"
#include "neuralnetwork/nnet.hpp"
#include "species.hpp"
#include "types.hpp"
#include <list>

typedef unsigned int uint;

extern pGenom PublicGenom;
extern std::list<NeuralNetwork> NNPopulation;
extern std::list<species> Species;
extern uint GlobalNodeCount, GlobalInputCount, GlobalOutputCount, GlobalNonHiddenNodeCount, PopulationSize, Innovations;
extern float WeightImportance, ExcessGeneImportance, DisjointGeneImportance, CompabilittyThreshhold, DeathRate;
