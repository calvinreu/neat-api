#pragma once
#include "species.hpp"
#include <iostream>

typedef float(*evaluation_func)(NeuralNetwork &nn);
extern float AverageScore; 
/*
 init the neat algorithm with every required value
 @param inputCount the number of input nodes
 @param outputCount the number of output nodes
 @param populationSize number of neural networks competing
 @param compabilittyThreshhold Threshhold of genetic difference to be part of a species
 @param weightImportance constant the weight difference is multiplied with when comparing two genoms
 @param excessGeneImportance constant/genom size for the difference while comparing two genoms
 @param disjointGeneImportance see excessGeneImportance
 @param deathRate the percent of creatures which are replaced each generation value should be between 0 and 1
*/
extern void Init(const uint &inputCount, const uint &outputCount, const uint &populationSize, const float &compabilittyThreshhold, const float &weightImportance, const float &excessGeneImportance, const float &disjointGeneImportance, const float &deathRate);

extern void NewGeneration();

extern void RunNNs(const uint &times, evaluation_func evaluate);
extern void Train(const uint &Generations, const uint &runsPerGen, evaluation_func evaluate);
extern void PrintScore(const uint &runCount, const uint &nnCount, evaluation_func evaluate);