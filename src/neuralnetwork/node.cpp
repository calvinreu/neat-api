#include "node.hpp"

void node::Trigger() {
    output = Sigmoid(input.Trigger());
}

void outputNode::Trigger() {
    *output = Sigmoid(input.Trigger());
}

float* node::GetOutputPtr(){
    return &output;
}

void node::AddLink(link &&link_) {
    input.links.push_back(link_);
}

void outputNode::AddLink(link &&link_) {
    input.links.push_back(link_);
}

void outputNode::SetOutput(float* output_) {
    output = output_;
}