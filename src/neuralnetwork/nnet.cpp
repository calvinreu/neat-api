#include "nnet.hpp"
#include <iostream>

const std::vector<float>& NeuralNetwork::GetOutput() {
    return outputVector;
}

const std::vector<float>& NeuralNetwork::Trigger() {
    for (auto i = hiddenNodes.begin(); i != hiddenNodes.end(); i++)
        i->Trigger();

    for (auto i = outputNodes.begin(); i != outputNodes.end(); i++)
        i->Trigger();
    
    return outputVector;
}

std::vector<float>* NeuralNetwork::GetInputVec() {
    return &inputVector;
}

NeuralNetwork::NeuralNetwork(const genom *genes) 
: inputVector(GlobalInputCount), outputVector(GlobalOutputCount), outputNodes(GlobalOutputCount), score(0) {
    std::multimap<uint, GeneData> nnetStructure;
    std::map<uint, node*> nodeReference;
    auto genomNodes = genes->getNodes();
    for (auto i = genes->getGenes().begin(); i != genes->getGenes().end(); i++)
        if(i->second.IsEnabled())
            nnetStructure.insert(std::make_pair(i->second.GetOut(), GeneData(i->second.GetWeight(), i->second.GetIn())));

    for (auto i = genomNodes.begin(); i != genomNodes.end(); i++)
        nodeReference.insert(std::make_pair(*i, nullptr));

    hiddenNodes.reserve(nodeReference.size());   

    for (auto it = nnetStructure.begin(); it->first < GlobalOutputCount+GlobalInputCount && it != nnetStructure.end(); it++)
        outputNodes[it->first-GlobalInputCount].AddLink(GetLink(it->second, nodeReference, nnetStructure));
    
    auto j = outputVector.begin();
    for (auto i = outputNodes.begin(); i != outputNodes.end(); i++, j++) {
        i->SetOutput(j.base());
    }
}

link NeuralNetwork::GetLink(const GeneData &geneData, std::map<uint, node*> &nodeReference, std::multimap<uint, GeneData> &nnetStructure) {
    if (geneData.in < GlobalInputCount)
        return link(geneData.weight, &inputVector[geneData.in]);

    auto itNode = nodeReference.find(geneData.in);
    
    if (itNode->second == nullptr) {
        node tempNode;
        for (auto i = nnetStructure.find(geneData.in); i->first == geneData.in && i != nnetStructure.end(); i++)
            tempNode.AddLink(GetLink(i->second, nodeReference, nnetStructure));
        hiddenNodes.push_back(tempNode);
        itNode->second = &hiddenNodes.back();
    }

    return link(geneData.weight, itNode->second->GetOutputPtr());
}

GeneData::GeneData(const float &weight, const uint &in)
:weight(weight), in(in) {}