#pragma once
#include "link.hpp"
#include "../util/sigmoid.hpp"
#include "../types.hpp"

class node
{
private:
    linkSet input;
    float output;
public:
    float* GetOutputPtr();
    void Trigger();
    void AddLink(link &&link_);
};

class outputNode
{
private:
    linkSet input;
    float* output;
public:
    void SetOutput(float *output);
    void Trigger();
    void AddLink(link &&link_);
};