#pragma once
#include "node.hpp"
#include <vector>
#include "../genom/genom.hpp"
#include <map>
#include "../types.hpp"
#include <set>

class NeuralNetwork
{
private:
    std::vector<node> hiddenNodes;
    std::vector<outputNode> outputNodes;
    std::vector<float> outputVector;
    std::vector<float> inputVector;
    link GetLink(const GeneData &geneData, std::map<uint, node*> &nodeReference, std::multimap<uint, GeneData> &nnetStructure);
public:
    float score;
    std::vector<float>* GetInputVec();
    const std::vector<float>& GetOutput();
    const std::vector<float>& Trigger();
    NeuralNetwork(const genom *genes);
};

struct GeneData
{
    float weight;
    uint in;
    GeneData(const float &weight, const uint &in);
};
