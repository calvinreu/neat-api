#include "link.hpp"

link::link(float weight, float* in) : weight(weight), in(in) {}

float linkSet::Trigger() {
    float output = 0;
    for (auto i = links.begin(); i != links.end(); i++) {
        output += (*i).weight * (*(*i).in);
    }

    return output;
}