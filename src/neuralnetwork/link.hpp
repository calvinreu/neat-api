#pragma once
#include <vector>
#include "../types.hpp"

struct link{
    float weight;
    float* in;
    link(float weight, float* in);
    link(const link &other):weight(other.weight){in = other.in;}
    link(link &&) { weight = std::move(weight), in = std::move(in); }
    ~link(){}
};

struct linkSet{
    std::vector<link> links;
    float Trigger();
    linkSet():links(){}
};
