# neat-api

api for neat which is supposed to have libs which make use of this api in at least golang, python and rust for c++ there should be a native version

[![pipeline status](https://gitlab.com/calvinreu/neat-api/badges/main/pipeline.svg)](https://gitlab.com/calvinreu/neat-api/-/commits/main)

## Support the project
if you want to help to contribute to this project look at the roadmap and choose a thing which is not in progress or write me an E-mail

## Contributing
all commits which pass the pipeline will be implemented note that the pipeline will get more suffisticated when the test system is done

## in progress

## done
 - add a pipeline and tests to check the validity of the commits 
 - create a cmake file to compile the code
 - create a functioning genom with all the options defined by the neat algorithm
 - create a function to construct a working neural network from a given genom
 - change the current neural network to use recursive function calls from the output nodes to the input nodes
 - implement crossover
 - implement species
 - implement selection
 - implement species reproduction
 - create functions which are easy to use to use this learning algorithm

## Roadmap
 - create command line calls for these functions with a in RAM file for the inputs as well as outputs of the neural networks
 - create interface libraries for the languages described above
 - write simple example files as well as a documentation for the api
 - write a more usefull readme

## License
This project is under the MIT license


## Project status
this project is under a lot of development but is in a very early stage in which it is difficult to test any parts since there is no working version yet

