#include "../src/neat.hpp"
#include <vector>
#include <iostream>

extern uint GlobalNodeCount, GlobalInputCount, GlobalOutputCount, GlobalNonHiddenNodeCount;

bool XOR(bool first, bool second) {
    return (first || second) && !(first && second);
}

float evaluation(NeuralNetwork &nn) {
    auto input = nn.GetInputVec();
    bool second = RandBool();
    bool first = RandBool();
    (*input)[2] = 1;//bias
    (*input)[0] = float(first);
    (*input)[1] = float(second);

    auto output = nn.Trigger();

    if ((XOR(first, second) && output[0] > 0.5) || ((!XOR(first, second)) && output[0] <= 0.5))
        return 1;
    
    return 0;
}

int main(int argc, char const *argv[])
{
    Init(3, 1, 1000, 0.2, 0.1, 1, 1, 0.8);
    PublicGenom.Print();
    PrintScore(100, 5, evaluation);

    for (size_t i = 0; i < 10; i++)
    {    
        Train(50, 5, evaluation);
        std::cout << i+1 << "0%\n";
    }
    

    PrintScore(100, 5, evaluation);
    std::cout << Species.size() << std::endl;
    //PublicGenom.Print();
    
    return 0;
}
